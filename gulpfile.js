const gulp = require("gulp")

// простое копирование файлов
gulp.task("css:copy", function () {
  return gulp
    .src('./src/css/*.css')
    .on('data', function (file) {
      console.log(file.path)
    })
    .pipe(gulp.dest('./build/css/'))
})

// чистим папку билда
const del = require("del")

gulp.task("clean", function (cb) {
  return del('./build/', cb);
})

// обьединение css
const concatCss = require("gulp-concat-css")

gulp.task("css:concat", function () {
  return gulp
    .src('./src/css/*.css')
    .pipe(concatCss("css/main.css"))
    .pipe(gulp.dest('./build/'))
})

// обьединение scss
const sass = require("gulp-sass");
const sassGlob = require('gulp-sass-glob');
const sourcemaps = require("gulp-sourcemaps");

gulp.task("scss:build", function () {
  return gulp
    .src('./src/css/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sassGlob())
    .pipe(sass({
      outputStyle: "compressed"
    }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./build/css'))
    .pipe(browserSync.stream())
})

const uglify = require('gulp-uglify');

// копирование js и минификация библиотек
gulp.task("js:copy", function () {
  return gulp
      .src('./src/js/libs/**/*.js')
      .pipe(uglify())
      .pipe(gulp.dest('./build/js/libs'))
})

// копирование html библиотек
gulp.task("html:copy", function () {
  return gulp
    .src('./src/*.html')
    .pipe(gulp.dest('./build/'))
})

const imagemin = require('gulp-imagemin');

// копирование image
gulp.task("image:min", function () {
  return gulp.src('src/image/*')
      .pipe(imagemin())
      .pipe(gulp.dest('./build/'))
})

// запуск локального серва

const browserSync = require("browser-sync").create();

gulp.task('browser-sync', function () {
  browserSync.init({
    server: {
      baseDir: "./build"
    }
  });
});

gulp.task('server:stream', function (cb) {
  browserSync.stream();
  cb();
});

// watcher

gulp.task('watch', function () {
  gulp.watch('./src/css/**/*.scss', gulp.series('scss:build'));
});


// серии задач
gulp.task('build', gulp.parallel("scss:build", "js:copy", "image:min", "html:copy"));
gulp.task('default', gulp.series("clean", "build", gulp.parallel("browser-sync", "watch")));
